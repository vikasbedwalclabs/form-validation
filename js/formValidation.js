$(document).ready(function() {
    var arr2 = [];
    $("#success").hide();
    $("#header [href]").click(function() {
        $("#main").slideDown("slow", function() {
            $("#header p").html("All Field are Mandatory");
        });
    });
    //  code for hiding the errorMsg when starts enter any info in fields
    $("*").keydown(function(e)
    {
        $('#error-msg').html("");
    });
    $(":checkbox").click(function()
    {
        $('#error-msg').html("");
    });
    $("#contact-contact-no").keydown(function(e)
    {
//code to format phone no. to contain "-"
        if (e.keyCode !== 8) {
            if ($(this).val().length === 0) {
                $(this).val($(this).val() + "(");
            }
            else if ($(this).val().length === 4) {
                $(this).val($(this).val() + ")-");
            } else if ($(this).val().length === 9) {
                $(this).val($(this).val() + "-");
            }
        }
    });
//    Code for styling the box border when user focus on it
    $("input").focus(function() {
        $(this).css({"border": "2px solid #dadada", "border-radius": "5px", "outline": "none", "border-color": "#9ecaed", "box-shadow": "0 0 10px #9ecaed"});
    });
//    code for change the css effect on blur action    
    $("input").blur(function() {
        $(this).css({"border": "1px solid black", "border-radius": "5px", "outline": "none", "border-color": "black", "box-shadow": "0 0 0 black"});
    });
//    Regular expression for email validation
    var emailFilter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
//    Regular expression for contact(US Based) validation
    var contactNo = /^[0-9-@!#\$\^%&*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]*$/;
    $('#button').click(function() {
//        First Name Validation
        var arr = [];
        arr = document.getElementById("first-name").value;
        for (i = 0; i <= (arr.length); i++) {
            if (!isNaN(arr[i]) || (($('#first-name').val()) === "")) {
                $('#error-msg').html("Enter valid First Name");
                document.getElementById("first-name").focus();
                return false;
                break;
            }
        }
//        Last name validation        
        var arr = [];
        arr = document.getElementById("last-name").value;
        for (i = 0; i <= (arr.length); i++) {
            if (!isNaN(arr[i]) || (($('#last-name').val()) === "")) {
                $('#error-msg').html("Enter valid Last Name");
                document.getElementById("last-name").focus();
                return false;
                break;
            }
        }
//         Date of Birth validation(as like restricted to select future date)
        if ($('#contact-dob').val() === '') {
            document.getElementById("error-msg").innerHTML = "Enter Valid DOB";
            document.getElementById("contact-dob").focus();
            return false;
        }
//          Adress field validation        
        else
        if ($('#contact-address').val() === '') {
            $('#error-msg').html("Enter Address");
            document.getElementById("contact-address").focus();
            return false;
        }
//       Contact no(US Based as like 222-444-9090) validation    
        else
            var contactNo = /^[0-9-()-]*$/;
        if (!contactNo.test($("#contact-contact-no").val()) || $("#contact-contact-no").val() === "" || null) {
            $('#error-msg').html("Enter valid Contact No");
            document.getElementById("contact-contact-no").focus();
            return false;
        }
//       Email validation    
        else
        if ($('#contact-email').val() === '' || !(emailFilter.test($('#contact-email').val()))) {
            $('#error-msg').html("Enter Valid Email");
            document.getElementById("contact-email").focus();
            return false;
        }
//        validation for a strong password    
        else
        if ($('#contact-password').val() === '' || $('#contact-password').val().length < 6) {
            $('#error-msg').html("Enter a Strong Password");
            document.getElementById("contact-password").focus();
            return false;
        }
//        Validation for confirm password field    
        else
        if ($('#contact-confirm-password').val() !== $('#contact-password').val()) {
            $('#error-msg').html("Password Mismatch");
            document.getElementById("contact-confirm-password").focus();
            return false;
        }
//         validation for selection of atleast one hobbies    
        else
        if ($('input[type=checkbox]:checked').length === 0) {
            $('#error-msg').html("Select atleast one Hobbie");
            document.getElementById("contact-hobbies").focus();
            return false;
        }
        $('#header').html('You have successfully submitted your information.');
        $("#main").slideUp(1000, function() {
            if (document.getElementById("contact-male").checked) {
                gender = "Male";
            }
            else {
                gender = "Female";
            }

            if (document.getElementById("checkbox1").checked && document.getElementById("checkbox2").checked) {
                hobbies = "PC Games, Net Surfing";
            }
            else
            if (document.getElementById("checkbox1").checked) {
                hobbies = "PC Games";
            }
            else {
                hobbies = "Net Surfing";
            }
            var arr2 = [$('#first-name').val(), $('#last-name').val(), gender, $('#contact-dob').val(), $('#contact-address').val(), $('#contact-contact-no').val(), $('#contact-email').val(), hobbies];
            $('#lbl-fname').html(arr2[0]);
            $('#lbl-lname').html(arr2[1]);
            $('#lbl-gender').html(arr2[2]);
            $('#lbl-dob').html(arr2[3]);
            $('#lbl-address').html(arr2[4]);
            $('#lbl-contact-no').html(arr2[5]);
            $('#lbl-email').html(arr2[6]);
            $('#lbl-hobbies').html(arr2[7]);
            $('#success').show();
        });
    });
    $('#edit-button').click(function() {
        $("#header").html("All Field are Mandatory.");
        $("#success").hide();
        $("#main").slideDown();
    });
    /*Date Constraint*/
    var dt = new Date();
    var yr = dt.getFullYear();
    var mn = (dt.getMonth()) + 1;
    if (mn.toString().length === 1) {
        mn = "0" + mn;
    }
    else {
        return mn;
    }
    var dy = dt.getDate();
    var maxvl = (yr + "-" + mn + "-" + dy);
    $("#contact-dob").attr("max", maxvl);
    $("#contact-dob").attr("value", maxvl);
});


    